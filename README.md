# LammpsSimAl_MigrationEnthalpy
In this project calculation of the Al migration enthalpy is done. Diffusion simulations were done using Lammps[1] package on the MIPT supercomputer. Python scripts analyze log- and dump- files of the simulations and produce the final result. 

[1] https://lammps.sandia.gov/
